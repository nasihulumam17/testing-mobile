import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\monic\\Downloads\\Android-MyDemoAppRN.1.2.0.build-231 (1).apk', true)

Mobile.tap(findTestObject('Object Repository/Login/android.view.ViewGroup'), 0)

Mobile.tap(findTestObject('Object Repository/Login/android.view.ViewGroup (1)'), 0)

Mobile.tap(findTestObject('Object Repository/Login/android.widget.EditText'), 0)

Mobile.sendKeys(findTestObject('Object Repository/Login/android.widget.EditText (1)'), 'nasihulumam17@gmail.com')

Mobile.tap(findTestObject('Object Repository/Login/android.widget.EditText (2)'), 0)

Mobile.setEncryptedText(findTestObject('Object Repository/Login/android.widget.EditText (3)'), 'qBt9ag4ue5g=', 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Object Repository/Login/android.view.ViewGroup (2)'), 0)

Mobile.closeApplication()

